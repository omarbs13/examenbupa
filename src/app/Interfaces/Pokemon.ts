export interface Pokemon {
    id: string;
    order: string;
    base_experience: string;
    weight: string;
  }