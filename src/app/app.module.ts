import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule }   from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserComponent } from './components/user/user.component';
import { PokemonComponent } from './components/pokemon/pokemon.component';
import { SearchUserComponent } from './components/search-user/search-user.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';

import { NgxPaginationModule } from 'ngx-pagination';
import { UserdetailComponent } from './components/userdetail/userdetail.component';
import { FilterPipe } from './pipes/filter.pipe';
@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    PokemonComponent,
    SearchUserComponent,
    NavbarComponent,
    UserdetailComponent,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgxPaginationModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
