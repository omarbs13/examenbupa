import { Component, OnInit } from '@angular/core';
import { PokemonService } from '../../services/pokemon-service.service';
import Swal from 'sweetalert2';
import { Pokemon } from 'src/app/Interfaces/Pokemon';
@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
})
export class PokemonComponent implements OnInit {
  pokemones: any[] = [];
  page: number;
  constructor(private service: PokemonService) {
    this.getPokemonList();
  }

  ngOnInit(): void {}

  getPokemonList() {
    this.service
      .getPokemonList()
      .subscribe((data) => (this.pokemones = data['results']));
  }

  verDetalle(nombre: string) {
    let pokemon: Pokemon;

    this.service.getPokemon(nombre).subscribe((data: any) => {
      pokemon = data;
      Swal.fire({
        title: `Pokemon: ${nombre} `,
        text: `Experiencia: ${pokemon.base_experience}, Peso: ${pokemon.weight} `,
      });
    });
  }
}


