import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from 'src/app/services/user-service.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { UsuarioModel } from 'src/app/models/usuariomodel';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit {
  users: any[] = [];
  page: number;
  public searchText: string;
  usuario: UsuarioModel = new UsuarioModel();
  @ViewChild('btnClose') btnClose;

  constructor(private service: UserService, private router: Router) {
    this.getUserList();
  }

  ngOnInit(): void {}

  getUserList() {
    this.service.getUserList().subscribe((data) => (this.users = data['data']));
  }

  verDetalle(id: string) {
    this.router.navigate(['/userdetail', id]);
  }

  edit(id: string) {
    this.service.getUser(id).subscribe((data) => {
      this.usuario.name = data['data'].first_name;
      this.usuario.id = data['data'].id;
    });
  }

  sendData(frm: NgForm) {
    if (frm.invalid) {
      return;
    }

    if (this.usuario.id) {
      this.service.editUser(this.usuario).subscribe((data: any) => {
        Swal.fire(`Se ha editado el usuario: ${data.name}
         con id: ${data.id}`);
        this.usuario = new UsuarioModel();
        document.getElementById('btnClose').click();
      });
    } else {
      this.service.postUser(this.usuario).subscribe((data: any) => {
        Swal.fire(`Se ha creado el usuario: ${data.name}
         con id: ${data.id}`);
        this.usuario = new UsuarioModel();
        document.getElementById('btnClose').click();
      });
    }
  }

  delete(id: string, name: string) {
    Swal.fire({
      title: 'Eliminar usuario',
      text: `Estas seguro de eliminar el usuario ${name}?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
    }).then((result) => {
      if (result.value) {
        this.service.delete(id);
        this.getUserList();
        Swal.fire('Eliminado!', 'El usuario ha sido eliminado.', 'success');
      }
    });
  }
}
