import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user-service.service';
import { User } from '../../Interfaces/User';

@Component({
  selector: 'app-userdetail',
  templateUrl: './userdetail.component.html',
})
export class UserdetailComponent implements OnInit {
  user: any = {};
  constructor(private route: ActivatedRoute, private service: UserService) {}

  ngOnInit(): void {
    this.route.params.subscribe((p) => {
      this.service
        .getUser(p['id'])
        .subscribe((data: any) => (this.user = data['data']));
    });
  }
}
