import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { urlPokemon } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  constructor(private http: HttpClient) {}

  private getQuery(query: string) {
    const url = `${urlPokemon.apiUrl}${query}`;
    return this.http.get(url);
  }

  getPokemonList() {
    return this.getQuery('pokemon?limit=100&offset=200');
  }

  getPokemon(name: string) {
    return this.getQuery(`pokemon/${name}`).pipe(map((data) => data));
  }
}
