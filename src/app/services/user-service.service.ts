import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { urlUser } from 'src/environments/environment';
import { UsuarioModel } from 'src/app/models/usuariomodel';
@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  private getQuery(query: string) {
    const url = `${urlUser.apiUrl}${query}`;
    return this.http.get(url);
  }

  private delQuery(query: string) {
    const url = `${urlUser.apiUrl}${query}`;
    return this.http.delete(url);
  }

  private postQuery(data: UsuarioModel) {
    const url = `${urlUser.apiUrl}`;
    return this.http.post(url, data);
  }

  getUserList() {
    return this.getQuery('');
  }

  getUser(id: string) {
    return this.getQuery(`/${id}`);
  }

  postUser(user: UsuarioModel) {
    const data = {
      ...user,
    };
    return this.postQuery(data);
  }

  editUser(user: UsuarioModel) {
    const data = {
      ...user,
    };
    return this.postQuery(data);
  }

  searchUser(name: string) {
    return this.getQuery('');
  }

  delete(id: string) {
    return this.delQuery(`/${id}`);
  }
}
